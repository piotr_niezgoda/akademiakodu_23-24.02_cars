package akademia.cars.repository;

import akademia.cars.model.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository //component controller, restController, service
public interface CarRepository extends JpaRepository<Car,Long> {

//    @Query(value = "select car from Car car where car.plate")    // JPQL
    @Query(value = "select * from car where plate = ?1", nativeQuery = true)    // sql
    Optional<Car> findCarByPlate(String plate);

}
