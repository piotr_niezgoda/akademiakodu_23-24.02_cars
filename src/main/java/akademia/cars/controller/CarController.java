package akademia.cars.controller;

import akademia.cars.exceptions.AlreadyExist;
import akademia.cars.exceptions.NotFoundException;
import akademia.cars.model.Car;
import akademia.cars.model.dtos.CarDTO;
import akademia.cars.service.CarService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class CarController {

    private CarService carService;

    // w nowym springu nie musi tytaj byc @Autowired
    public CarController(CarService carService) {
        this.carService = carService;
    }


    @GetMapping("/cars")
    @CrossOrigin
    public List<Car> getCars(){
        return carService.getCars();
    }

    //    @RequestMapping(value = "/cars", method = RequestMethod.GET)  // starsza adnotacja
    @GetMapping("/cars/{plate}")
    @CrossOrigin
    public Car getCar(@PathVariable String plate){
        return carService.getCarByPlate(plate);
    }

    //DTO
    @GetMapping("/dto/cars")
    @CrossOrigin
    public List<CarDTO> getCarsDTO(){
        return carService.getCarsDto();
    }

    @PostMapping("/dto/cars")
    public ResponseEntity<?> addCar(@RequestBody CarDTO carDTO) throws AlreadyExist {
        if (!carService.addCar(carDTO)){
            return new ResponseEntity<>(HttpStatus.CONFLICT); // 409
        } else {
            return new ResponseEntity<>(HttpStatus.CREATED);   // 201
        }
    }

    @PostMapping("/dto/cars/param")         // cwiczebny
    public ResponseEntity<?> addCarParams(@RequestParam String brand, @RequestParam String model, @RequestParam String power, @RequestParam String plate) throws AlreadyExist {
        if (!carService.addCar(new CarDTO(brand,model,power,plate))){
            return new ResponseEntity<>(HttpStatus.CONFLICT); // 409
        } else {
            return new ResponseEntity<>(HttpStatus.CREATED);   // 201
        }
    }

   /* @DeleteMapping("/dto/cars/{plate}")
    public ResponseEntity<String> deleteCar(@PathVariable String plate) throws NotFoundException {
        carService.deleteCar(plate);
        return new ResponseEntity<>(HttpStatus.OK);
    }*/

    @DeleteMapping("/dto/cars/{plate}")
    public ResponseEntity<?> deleteCar(@PathVariable String plate) throws NotFoundException {
        if (carService.deleteCar(plate)){
            return new ResponseEntity<>(HttpStatus.OK);
        } else {
//            throw new NotFoundException("Car not exist");     // jeden sposob albo drugi
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/dto/cars/{plate}")
    public CarDTO updateCarByPlate(@PathVariable(value = "plate") String plate, @RequestBody CarDTO carDTO) throws NotFoundException {
        return carService.updateCarByPlate(plate,carDTO);
    }

}
