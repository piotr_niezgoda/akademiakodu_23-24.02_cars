package akademia.cars.controller;

import akademia.cars.exceptions.AlreadyExist;
import akademia.cars.exceptions.NotFoundException;
import akademia.cars.model.dtos.CarDTO;
import akademia.cars.service.CarService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/")
public class HomeController {

    private CarService carService;

    // w nowym springu nie trzeba autowired
    public HomeController(CarService carService) {
        this.carService = carService;
    }

    @RequestMapping("home")
    public String getHomePage(Model model){
        String welcome = "Welcome to my awesome Cars App!";
        model.addAttribute("welcome", welcome);
        model.addAttribute("cars", carService.getCars());
        return "index";
    }

    @PostMapping("add")
    public String addCar(@Valid @ModelAttribute CarDTO carDTO) throws AlreadyExist {

        carService.addCar(carDTO);

        return "redirect:/home";
    }

    @PostMapping("update")
    public String updateCar(@Valid @ModelAttribute CarDTO carDTO, @RequestParam(value = "plate") String plate) throws NotFoundException {

        carService.updateCarByPlate(plate,carDTO);

        return "redirect:/home";
    }

    @PostMapping("redirect/update")
    public String getRedirectToUpdate(@RequestParam(value = "plate") String plate, Model model){

        model.addAttribute("carDTO",carService.getCarDtoByPlate(plate));

        return "update";
    }


    @GetMapping("del")
    public String deleteCar(@RequestParam(value = "plate", required = false) String plate) throws NotFoundException {
        carService.deleteCar(plate);
        return "redirect:/home";
    }
}
