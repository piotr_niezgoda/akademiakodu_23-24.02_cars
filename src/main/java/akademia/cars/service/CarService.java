package akademia.cars.service;

import akademia.cars.exceptions.AlreadyExist;
import akademia.cars.exceptions.NotFoundException;
import akademia.cars.mappers.CarMapper;
import akademia.cars.model.Car;
import akademia.cars.model.dtos.CarDTO;
import akademia.cars.repository.CarRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CarService {

    private CarRepository carRepository;
    private CarMapper carMapper;

    // DI - Dependency Injection // Adn, Autowired jest nie wymagana


    public CarService(CarRepository carRepository, CarMapper carMapper) {
        this.carRepository = carRepository;
        this.carMapper = carMapper;
    }

    public List<Car> getCars(){
        return carRepository.findAll();
    }

    public Car getCarByPlate(String plate){
        return carRepository.findCarByPlate(plate).get();
    }

    public Optional<Car> getCarByPlateOptional(String plate){
        return carRepository.findCarByPlate(plate);
    }

    //DTO
    public List<CarDTO> getCarsDto(/*String plate*/){
        List<CarDTO> carDTOS = new ArrayList<>();

        for (Car car : carRepository.findAll()) {
            carDTOS.add(carMapper.map(car));
        }

        return carDTOS;
    }

    public CarDTO getCarDtoByPlate(String plate){
        return carMapper.map(carRepository.findCarByPlate(plate).get());
    }

    public boolean deleteCar(String plate) throws NotFoundException {

        Optional<Car> car = carRepository.findCarByPlate(plate);

        if (car.isPresent()){
            carRepository.deleteById(car.get().getId());    // pierwsze get, bo wypakowujemy z Optionala
            return true;
        } else {
            return false;
        }
    }

    public boolean addCar(CarDTO carDTO) throws AlreadyExist{
        Optional<Car> car = carRepository.findCarByPlate(carDTO.getPlate());

        if (car.isPresent()){
            throw  new AlreadyExist("Car exist!");
        } else {
//            Car carDAO = new Car(carDTO.getBrand(), carDTO.getModel(), carDTO.getPower(), carDTO.getPlate());
//            carRepository.save(carDAO);

            // albo to (builder), albo to wyzej. To z builderem nie potrzebuje konstruktora bez pola Id, jak to wyzej
            carRepository.save(Car.builder().brand(carDTO.getBrand()).model(carDTO.getModel()).power(carDTO.getPower()).plate(carDTO.getPlate()).build());
            return true;
        }


//        Optional.of(carRepository.findCarByPlate(carDTO.getPlate()).orElseGet(() -> {
//            carRepository.findCarByPlate(carDTO.getPlate()).ifPresent(d -> {});
//        }));
    }

    public CarDTO updateCarByPlate(String plate, CarDTO carDTO) throws NotFoundException{

        Optional<Car> car = carRepository.findCarByPlate(plate);

        if (car.isPresent()){

            car.get().setBrand(carDTO.getBrand());
            car.get().setModel(carDTO.getModel());
            car.get().setPower(carDTO.getPower());
            car.get().setPlate(carDTO.getPlate());

            carRepository.save(car.get());
            return carMapper.map(car.get());
        } else {
            throw new NotFoundException("Car not exist");
        }
    }
}
